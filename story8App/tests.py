from django.test import TestCase
from django.test import Client
from django.apps import apps
from .apps import Story8AppConfig
from django.urls import resolve, reverse
from . import views
from .views import bookSearchView, bookAPI
import json

class AppTest(TestCase):   
    def test_apps(self):
        self.assertEqual(Story8AppConfig.name, 'story8App')
        self.assertEqual(apps.get_app_config('story8App').name, 'story8App')
        
class UrlsTest(TestCase):
    def test_url_landing_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_landing_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, "index.html")
    
    def test_bookSearch_use_right_function(self):
        self.bookSearch = reverse("story8App:bookSearch")
        found = resolve(self.bookSearch)
        self.assertEqual(found.func, views.bookSearchView)

    def test_index_url(self):
        found = resolve(reverse('story8App:ajax'))
        self.assertTrue(found.func, views.bookAPI)

class ViewsTest(TestCase):
    def test_setup(self):
        self.client = Client()
        self.response = self.client.get('/story8/')
        self.page_content = self.response.content.decode('utf8')
    
    def test_GET_bookSearch(self):
        self.bookSearch = reverse("story8App:bookSearch")
        response = self.client.get(self.bookSearch)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'story8/index.html')

    def test_book_api(self):
        found = resolve(reverse('story8App:ajax'))
        self.assertTrue(found.func, views.bookAPI)

    def test_json(self):
        response = self.client.get('/story8/ajax/?q=test')
        data = json.loads(response.content)
        self.assertGreater(len(data['items']), 0)
