from django.urls import path
from . import views

app_name = "story8App"

urlpatterns = [
    
    path('', views.bookSearchView, name='bookSearch'),
    path('ajax/', views.bookAPI, name='ajax')
]


