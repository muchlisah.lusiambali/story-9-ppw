from django.urls import path

from .views import (
    index
)

app_name = "story7App"
urlpatterns = [
    path('',index, name="index")
]